package com.khata.khata.repository;

import com.khata.khata.entity.Billing;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Billingrepository extends JpaRepository<Billing, Long> {
}
