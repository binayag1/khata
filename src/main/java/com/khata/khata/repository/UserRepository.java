package com.khata.khata.repository;

import com.khata.khata.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends CrudRepository<User, Integer> {

    @Query(value = "SELECT * from USER u WHERE u.email = :email", nativeQuery = true)
    public User getUserByEmail(@Param("email") String email);

}
