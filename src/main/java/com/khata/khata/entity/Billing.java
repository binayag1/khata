package com.khata.khata.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
public class Billing {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String itemdetails;
    private String qty;
    private String price;
    private String deduction;
    private String total;
    private String purchasedby;
    private String purchasedfrom;
    private String enteredby;

    @Column(nullable = false)
    int projectid;
    String projectname;

    @DateTimeFormat(pattern = "dd/mm/yyyy")
    Timestamp entrydate;

    public Billing(){

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemdetails() {
        return itemdetails;
    }

    public void setItemdetails(String itemdetails) {
        this.itemdetails = itemdetails;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDeduction() {
        return deduction;
    }

    public void setDeduction(String deduction) {
        this.deduction = deduction;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public Timestamp getEntrydate() {
        return entrydate;
    }

    public void setEntrydate(Timestamp entrydate) {
        this.entrydate = entrydate;
    }

    public String getPurchasedby() {
        return purchasedby;
    }

    public void setPurchasedby(String purchasedby) {
        this.purchasedby = purchasedby;
    }

    public String getPurchasedfrom() {
        return purchasedfrom;
    }

    public void setPurchasedfrom(String purchasedfrom) {
        this.purchasedfrom = purchasedfrom;
    }

    public String getEnteredby() {
        return enteredby;
    }

    public void setEnteredby(String enteredby) {
        this.enteredby = enteredby;
    }

    public int getProjectid() {
        return projectid;
    }

    public void setProjectid(int projectid) {
        this.projectid = projectid;
    }

    public String getProjectname() {
        return projectname;
    }

    public void setProjectname(String projectname) {
        this.projectname = projectname;
    }
}
