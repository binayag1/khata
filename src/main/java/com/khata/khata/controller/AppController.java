package com.khata.khata.controller;

import com.khata.khata.UserAuthorization.MyUserDetails;
import com.khata.khata.entity.Billing;
import com.khata.khata.entity.Project;
import com.khata.khata.entity.User;
import com.khata.khata.service.Billingservice;
import com.khata.khata.service.ProjectService;
import com.khata.khata.service.UserService;
import com.sun.security.auth.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;
import java.sql.Timestamp;
import java.util.List;

@Controller
public class AppController {

    @Autowired
    private Billingservice billingservice;

    @Autowired
    private UserService userService;

    @Autowired
    private ProjectService projectService;

    /**
     *
     * @param model
     * @return
     */
    @RequestMapping("/")
    public String viewHomePage(Model model){
        List<Billing> allBills = billingservice.listAll();
        model.addAttribute("allbilling",allBills);
        return "index";
    }


    /**
     * Controllers Related to Billing Information
     */

    /**
     *
     * @param model
     * @return
     */
    @RequestMapping("/view_billing_information")
    public String viewBillingInformation(Model model){
        List<Billing> allBills = billingservice.listAll();
        model.addAttribute("allbilling",allBills);
        return "view_billing_information";
    }

    /**
     *
     * @param model
     * @return
     */
    @RequestMapping("/new")
    public String showNewBillingForm(Model model){
        Billing billing = new Billing();
        model.addAttribute("billing",billing);
        return "new_billing";
    }

    /**
     *
     * @param billing
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveBillingInformation(@ModelAttribute("billing") Billing billing, RedirectAttributes redirectAttributes
    ,Principal principal
    ){
        billing.setEntrydate(new Timestamp(System.currentTimeMillis()));//Set Current time here

        final String email = SecurityContextHolder.getContext().getAuthentication().getName();


        if(email != null)
        System.out.println("Email :------------------------>>>> "+email);

        User user = userService.getUserByEmail(email);
        int projectId = user.getProjectid();

        Project project = projectService.getProject(projectId);
        String projectName = project.getProjectname();

        //Set the project name in billing
        billing.getProjectname();

        //Save billing-information
        billingservice.save(billing);
        redirectAttributes.addFlashAttribute("successmessage","Data Saved Successfully");
        return "redirect:/new";
    }

    /**
     *
     * @param id
     * @return
     */
    @RequestMapping("/edit/{id}")
    public ModelAndView showEditBillingFrom(@PathVariable(name="id") Long id){
        ModelAndView mav = new ModelAndView("edit_billing");
        Billing billing = billingservice.get(id);
        mav.addObject("billing", billing);
        return mav;
    }

    /**
     *
     * @param id
     * @return
     */
    @RequestMapping("/delete/{id}")
    public String deleteBillingFrom(@PathVariable(name="id") Long id){
        billingservice.delete(id);
        return "redirect:/view_billing_information";
    }

    /**
     * Controllers for Registration page (Users)
     */

    /**
     *
     * @param model
     * @return
     */
    @RequestMapping("/admin/register")
    public String showRegisterPage(Model model){

        User user = new User();
        model.addAttribute("user",user);

        List<Project> allProjects = projectService.listAllProject();

        for (Project project: allProjects){
            System.out.println("Project Name : "+project.getProjectname());
        }

        model.addAttribute("allprojects", allProjects);

        return "admin/register";
    }

    /**
     *
     * @param user
     * @return
     */
    @PostMapping("/admin/register")
    public String saveUserData(@ModelAttribute("user") @Validated User user, Model model, BindingResult bindingResult){

        if(bindingResult.hasErrors()){
            List<Project> allProjects = projectService.listAllProject();
            model.addAttribute("allprojects", allProjects);
            return "admin/register";

        }

        //TO- DO validation part
        try{

            Project project = projectService.getProject(user.getProjectid());
            String projectName = project.getProjectname();
            user.setProjectname(projectName);

            userService.saveUser(user);
        }catch (Exception e){

            if(bindingResult.hasErrors()){
                List<Project> allProjects = projectService.listAllProject();
                model.addAttribute("allprojects", allProjects);
                model.addAttribute("errorMessage", "Error: " + e.getMessage());
                return  "admin/register";
            }
        }

        return "admin/register_success";

    }

    @RequestMapping("/admin/view_users_information")
    public String showAllUsers(Model model){

        List<User> allUsers = userService.listAllUsers();
        model.addAttribute("allusers",allUsers);
        return "admin/view_users_information";

    }


    /*
    * Controller related to Project Information
    * Only Authorized person can create the project and assign project to related person
    */

    @RequestMapping("/admin/new_project")
    public String showCreateProject(Model model){
        Project project = new Project();
        model.addAttribute("project",project);
        return "admin/new_project";
    }

    /**
     *
     * @param project
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = "/admin/save_project", method = RequestMethod.POST)
    public String saveProjectInformation(@ModelAttribute("project") Project project, RedirectAttributes redirectAttributes){

        project.setEntryDate(new Timestamp(System.currentTimeMillis())); //Set Current Time Here

        if(projectService.save(project) == 1){
            redirectAttributes.addFlashAttribute("successmessage","Data Saved Successfully.");
        }else{
            redirectAttributes.addFlashAttribute("errormessage","Error Saving Data.");
        }

        return "redirect:admin/new_project";
    }

    @RequestMapping("/admin/view_project_information")
    public String displayAllProjectInformation(Model model){

        List<Project> listOfAllProjects = projectService.listAllProject();
        model.addAttribute("allprojects", listOfAllProjects);
        return "admin/view_project_information";

    }

    @RequestMapping("/admin/edit_project/{project_id}")
    public ModelAndView editProject(@PathVariable(name = "project_id" ) Integer id){

        ModelAndView mav = new ModelAndView("admin/edit_project");
        Project project = projectService.getProject(id);
        mav.addObject("project",project);
        return mav;

    }

    /**
     * Controllers related to Search Page
     *  to-do - Logic Implementation
     */

    /**
     *
     * @return
     */
    @RequestMapping("/search")
    public String returnHeadSection(){

        return "search";
    }

    /**
     * Admin Controllers
     */
    @RequestMapping("/admin/admin_panel")
    public String showAdminPanel(){
        return "admin/admin_panel";
    }

}
