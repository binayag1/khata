package com.khata.khata.UserAuthorization;

import com.khata.khata.entity.User;
import com.khata.khata.repository.UserRepository;
import com.khata.khata.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UserDetailsServiceImplementation implements UserDetailsService {

    @Autowired
    UserService userService;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        User user = userService.getUserByEmail(email);

        if(user == null){
            throw new UsernameNotFoundException("Could Not Find User");
        }

        return new MyUserDetails(user);
    }
}
