package com.khata.khata.service;

import com.khata.khata.entity.Billing;
import com.khata.khata.repository.Billingrepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Billingservice {

    @Autowired
    private Billingrepository billingrepository;

    public List<Billing> listAll(){
        return billingrepository.findAll();
    }

    public void save(Billing billing){
        billingrepository.save(billing);
    }

    public Billing get(Long id){
        return billingrepository.findById(id).get();
    }

    public void delete(Long id){
        billingrepository.deleteById(id);
    }


}
