package com.khata.khata.service;

import com.khata.khata.entity.Project;
import com.khata.khata.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectService {

    @Autowired
    ProjectRepository projectRepository;

    //List out all project in the database
    public List<Project> listAllProject(){
        return projectRepository.findAll();
    }

    //Save project
    public int save(Project project){
        if(projectRepository.save(project) != null){
            return 1;
        }else{
            return 0;
        }
    }

    //Get details of one project
    public Project getProject(int id){
        return projectRepository.getOne(id);
    }

    //Delete project
    public void deleteProject(int id){
       projectRepository.deleteById(id);
    }


}
